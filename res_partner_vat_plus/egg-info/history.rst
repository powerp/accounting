12.0.1.0.3 (2021-04-13)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] impostati i test con relativa validazione

12.0.1.0.2 (2021-04-08)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] aggiornamento documentazione: in fase di creazione, modifica e cambio di partita iva per clienti o fornitori i primi due caratteri vengono impostati in maiuscolo

12.0.1.0.1 (2021-04-08)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Impostato test

