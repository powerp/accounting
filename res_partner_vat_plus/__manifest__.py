#
# Copyright (c) 2021
#
{
    'name': 'Partner VAT capitalize',
    'summary': 'Partner VAT capitalize first and second characters',
    'version': '12.0.1.0.3',
    'category': 'Accounting',
    'author': 'powERP, Didotech srl, SHS-AV srl',
    'website': 'www.powerp.it',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'base_vat',
    ],
    'data': [],
    'installable': True,
}
