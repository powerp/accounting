Chart Of Account Hierarchy
--------------------------

This module enables the Chart of Account Hierarchy, like in previous Odoo version before 10.0.
You can manage 3 levels of hierarchy, setting number of characters of every level.

In the top level of Chart of Account you can declare the nature of the record, which may be:

* Asset
* Liability
* Revenue
* Expense
* Loss & Profit

to manage classical balance sheets.

This module enable the account type menu that is hidden in Odoo standard.