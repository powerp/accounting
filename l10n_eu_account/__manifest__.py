#
# Copyright 2019-20 - SHS-AV s.r.l. <https://www.zeroincombenze.it/>
#
# Contributions to development, thanks to:
# * Antonio Maria Vigliotti <antoniomaria.vigliotti@gmail.com>
#
# License LGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
#
{
    'name': 'Transnational Account',
    'version': '12.0.0.1.1',
    'category': 'Accounting',
    'summary': 'Replace standard Odoo validation',
    'author': 'powERP, Didotech srl, SHS-AV srl, SHS-AV s.r.l.',
    'website': 'https://www.powerp.it',
    'license': 'LGPL-3',
    'depends': [
        'account',
        'base',
    ],
    'data': ['views/account_view.xml'],
    'installable': True,
}
