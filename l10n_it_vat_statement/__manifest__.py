# Copyright 2011-21 domsense <http://https://www.domsense.com>
# Copyright 2012-21 agilebg <http://https://www.agilebg.com>
# Copyright 2012-21 linkgroup <http://https://https://www.linkgroup.it>
# Copyright 2015-21 odoo-italia <http://https://www.odoo-italia.org>
# Copyright 2020-21 SHS-AV s.r.l. <https://www.shs-av.com>
#
# Copyright 2021 powERP, Didotech srl, SHS-AV srl <https://www.powerp.it>
# Copyright 2021 Odoo Community Association (OCA) <https://odoo-community.org>
# Copyright 2021 SHS-AV s.r.l. <https://www.zeroincombenze.it>
#
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
#
{
    "name": "ITA - Liquidazione IVA evoluta",
    "version": "12.0.1.6.6",
    'category': 'Localization/Italy',
    'summary': "Allow to create the 'VAT Statement'.",
    'license': 'AGPL-3',
    "author": "Agile Business Group, Odoo Community Association (OCA)"
              ", LinkIt Spa, powERP",
    'website': 'https://github.com/OCA/l10n-italy/tree/12.0/',
    "depends": [
        "account",
        "account_tax_balance",
        "date_range",
        "l10n_it_account",
        "l10n_it_fiscalcode",
        "web",
        "l10n_it_account_tax_kind",
        "account_invoice_entry_dates",
        "account_move_plus",
        ],
    'data': [
        'wizard/add_period.xml',
        'wizard/remove_period.xml',
        'security/ir.model.access.csv',
        'security/security.xml',
        'report/reports.xml',
        'views/report_vatperiodendstatement.xml',
        'views/config.xml',
        'views/account_view.xml',
        'views/account_move_view.xml',
    ],
    'installable': True,
}
