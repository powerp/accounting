12.0.1.6.6 (2021-04-09)
~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Corretto bug generazione stampa liquidazione

12.0.1.6.5 (2021-04-07)
~~~~~~~~~~~~~~~~~~~~~~~

* [REF] Aggiornate dipendenze

12.0.1.6.4 (2021-03-15)
~~~~~~~~~~~~~~~~~~~~~~~

* [REF] Refactoring gestione data competenza IVA


12.0.1.6.3 (2020-12-02)
~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] POW-106 Data competenza IVA da data registrazione, se vuota


12.0.1.6.2 (2020-12-02)
~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] POW-92 Corretto errore calcolo liquidazione IVA per data competenze


12.0.1.6.1 (2020-11-27)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Inserita la verifica sulla data fattura e la data di applicazione iva

