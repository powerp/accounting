#
# Copyright (c) 2020
#
{
    "name": "Account Invoice Constraint Chronology No Draft",
    "version": "12.0.1.1.2",
    "category": "Accounting",
    "summary": "Validate using check on invoice not in state 'draft' ",
    "author": "Powerp Srl",
    "website": "",
    "license": "LGPL-3",
    "depends": [
        "account",
    ],
    "data": ['views/account_view.xml'],
    'installable': True,
    'pre_init_hook': 'pre_init_hook',
}
