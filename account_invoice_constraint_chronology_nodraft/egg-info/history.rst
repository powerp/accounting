12.0.1.1.2 (2021-03-16)
~~~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] remove optimization check always the date

12.0.1.1.1 (2021-02-24)
~~~~~~~~~~~~~~~~~~~~~~~~~~

* [REF] refactoring using date into domain check
