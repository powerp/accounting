# -*- coding: utf-8 -*-
{
    'name': "SDI: 2C Solution",

    'summary': """
        Access 2C Solution SDI""",

    'description': """
        Module gives functions necessary to access 2C Solution SDI provider
    """,

    'author': "Didotech s.r.l.",
    'website': "http://www.didotech.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Generic Modules/Accounting',
    'version': '0.4.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'l10n_it_efattura_sdi'
    ],

    # always loaded
    'data': [
        'data/2c_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ]
}
