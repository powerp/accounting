#
# Copyright 2020
#
{
    'name': 'account_invoice_zero_amount',
    'summary': 'Account invoice zero amount',
    'version': '12.0.0.1.0',
    'category': 'Generic Modules/Accounting',
    'author': 'Didotech s.r.l. / SHS-AV s.r.l.',
    'website': '',
    'depends': ['base',
                'account',
    ],
    'data': [
        # 'security/ir.model.access.csv',
        'views/account_invoice_button.xml',
        'wizard/wizard_confirm.xml',
    ],
    'installable': True,
}
