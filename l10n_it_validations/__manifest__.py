#
# Copyright 2021 powERP, Didotech srl, SHS-AV srl <https://www.powerp.it>
# Copyright 2020-21 SHS-AV s.r.l. <https://www.zeroincombenze.it>
# Copyright 2020-21 Didotech s.r.l. <https://www.didotech.com>
#
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
#
{
    'name': 'Account validations',
    'summary': 'Account validation for Italian Localization',
    'version': '12.0.1.8.23',
    'category': 'Accounting',
    'author': 'SHS-AV s.r.l.',
    'website': 'https://www.zeroincombenze.it/',
    'license': 'LGPL-3',
    'depends': [
        'account',
        'base',
        'date_range',
        'account_invoice_entry_dates',
        'account_move_plus',
        'account_invoice_13_more',
    ],
    'data': [
        'views/account_invoice_view.xml',
    ],
    'installable': True,
}
