# Copyright 2020-2016 Powerp Enterprise Network
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    'name': 'Account RiBa CBI',
    'summary': 'Gestione Ri.Ba.',
    'version': '12.0.3.5.12',
    'license': 'AGPL-3',
    'author': 'Powerp Enterprise Network',
    'website': 'http://www.powerp.it',
    'category': 'Banking addons',
    'depends': [
        'base',
        'account',
        'account_duedates',
        'account_payment_order',
        'account_payment_mode',
        'account_banking_common',
    ],
    'external_dependencies': {
        'python': [
            'ribalta',
        ]
    },
    'data': [
        'views/res_config.xml',
        'views/account_payment_mode.xml',
        'views/account_bank_journal_form.xml',
        'data/account_payment_method.xml',
        'views/res_partner_bank_form_view.xml',
    ],
    'installable': True,
}
