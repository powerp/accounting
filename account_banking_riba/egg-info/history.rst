12.0.3.5.12 (2021-04-15)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Aggiornato calcoli conti degli effetti

12.0.3.5.11 (2021-04-15)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Impostato castelletto

12.0.3.4.10 (2021-02-15)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] No riferimento a data bilancio

12.0.3.4.9 (2021-02-02)
~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Nuova implementazione registrazione contabile di incasso effettivo Ri.Ba.

12.0.2.3.9 (2021-02-02)
~~~~~~~~~~~~~~~~~~~~~~~

* [REF] Refactoring

12.0.2.3.8 (2021-02-01)
~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Fix bug accredito

12.0.2.3.7 (2021-01-25)
~~~~~~~~~~~~~~~~~~~~~~~

* [REF] Refactoring

12.0.2.3.5 (2021-01-07)
~~~~~~~~~~~~~~~~~~~~~~~

12.0.2.3.6 (2021-01-08)
~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] impostato metodo di accredito

12.0.2.3.5 (2021-01-07)
~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] modificata regola validazione codice SIA

12.0.2.2.5 (2021-01-07)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Added dependency account_duedates module

12.0.0.2.1 (2020-12-07)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] CAB and ABI taken directly from IBAN code, sia code '00000' accepted

12.0.0.0.1 (2020-12-07)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] First release of the module: CBI files generation and SIA code settings are available
