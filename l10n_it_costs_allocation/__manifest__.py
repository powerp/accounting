#
# Copyright (c) 2021
#
{
    'name': 'Suddivisione dei costi',
    'version': '12.0.0.1.18',
    'category': 'Accounting',
    'summary': 'Suddivisione dei costi',
    'author': 'powERP',
    'website': 'https://www.powerp.it',
    'license': 'LGPL-3',
    'depends': [
        'account',
        'sale',
        'product',
        'base',
    ],
    'data': [
        'views/product_views.xml',
        'views/sale_views.xml',
        'views/invoice_views.xml',
    ],
    'installable': True,
}
