# Copyright 2020-21 powERP, Didotech srl, SHS-AV srl <https://www.powerp.it>
# Copyright 2020-21 SHS-AV s.r.l. <https://www.zeroincombenze.it>
#
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
#
{
    'name': 'Account move line type',
    'version': '12.0.1.0.3',
    'category': 'Accounting & Finance',
    'author': 'Didotech srl',
    'website': 'https://www.didotech.com/',
    'license': 'LGPL-3',
    'summary': 'Add line type in move lines',
    'depends': [
        'account',
    ],
    'data': [
        'views/account_move_line_view.xml',
    ],
    'installable': True,
}
