12.0.1.0.3 (2021-04-09)
~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Aggiornato il metodo che calcolava il tipo di linea gestendo il caso con più codici iva

12.0.1.0.2 (2021-02-19)
~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Funzione get_line_type

12.0.1.0.1 (2021-02-18)
~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Funzione get_line_type
