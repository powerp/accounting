#
# Copyright 2020-2016 Powerp Enterprise Network
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
#
{
    'name': 'Account banking invoice financing',
    'summary': 'Account banking invoice financing',
    'version': '12.0.7.9.9',
    'category': 'Accounting',
    'author': 'PowERP',
    'website': 'https://www.powerp.it',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'account',
        'account_due_list',
        'account_duedates',
        'account_payment_order',
        'account_payment_mode',
        'account_banking_common',
        'account_invoice_13_more',
    ],
    'data': [
        'views/account_due_list_view.xml',
        'views/account_payment_order_form_view.xml',
        'views/res_partner_bank_form_view.xml',
        'views/account_view_move_line_form.xml',
        'views/account_bank_journal_form.xml',
        'reports/print_account_payment_order_ita.xml',
        'reports/account_payment_order_financing_report.xml',
        'data/account_payment_method.xml',
        'wizard/wizard_payment_order_invoice_financing.xml',
        'wizard/wizard_payment_order_close_financing.xml',
    ],
    'installable': True,
    'maintainer': 'powERP'
}
