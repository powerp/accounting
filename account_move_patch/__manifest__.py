# Copyright 2020 SHS-AV s.r.l. <https://www.zeroincombenze.it>
# Copyright 2020 Didotech s.r.l. <https://www.didotech.com>
#
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
#
{
    'name': 'Account Patch',
    'summary': 'Patches to Odoo Invoicing (account) module',
    'version': '12.0.0.0.1',
    'category': 'Uncategorized',
    'website': 'https://www.powerp.it',
    'author': 'powERP',
    'license': 'LGPL-3',
    'installable': True,
    'depends': [
        'base',
        'account',
    ],
    'data': [
    ],
    'development_status': 'Mature',
}
