
===================================
|icon| l10n_it_mastrini 12.0.8.7.22
===================================


**Mastrini**

.. |icon| image:: https://raw.githubusercontent.com/powerp/accounting/12.0/l10n_it_mastrini/static/description/icon.png

|Maturity| |Build Status| |Codecov Status| |license gpl| |Try Me|


.. contents::


Overview / Panoramica
=====================

|en| Mastrini.

|

|it| Mastrini.

|
|

Getting started / Come iniziare
===============================

|Try Me|


|

Installation / Installazione
----------------------------


+---------------------------------+------------------------------------------+
| |en|                            | |it|                                     |
+---------------------------------+------------------------------------------+
| These instructions are just an  | Istruzioni di esempio valide solo per    |
| example; use on Linux CentOS 7+ | distribuzioni Linux CentOS 7+,           |
| Ubuntu 14+ and Debian 8+        | Ubuntu 14+ e Debian 8+                   |
|                                 |                                          |
| Installation is built with:     | L'installazione è costruita con:         |
+---------------------------------+------------------------------------------+
| `Zeroincombenze Tools <https://zeroincombenze-tools.readthedocs.io/>`__    |
+---------------------------------+------------------------------------------+
| Suggested deployment is:        | Posizione suggerita per l'installazione: |
+---------------------------------+------------------------------------------+
| $HOME/12.0                                                                 |
+----------------------------------------------------------------------------+

::

    cd $HOME
    # *** Tools installation & activation ***
    # Case 1: you have not installed zeroincombenze tools
    git clone https://github.com/zeroincombenze/tools.git
    cd $HOME/tools
    ./install_tools.sh -p
    source $HOME/devel/activate_tools
    # Case 2: you have already installed zeroincombenze tools
    cd $HOME/tools
    ./install_tools.sh -U
    source $HOME/devel/activate_tools
    # *** End of tools installation or upgrade ***
    # Odoo repository installation; OCB repository must be installed
    odoo_install_repository accounting -b 12.0 -O powerp -o $HOME/12.0
    vem create $HOME/12.0/venv_odoo -O 12.0 -a "*" -DI -o $HOME/12.0

From UI: go to:

* |menu| Setting > Activate Developer mode 
* |menu| Apps > Update Apps List
* |menu| Setting > Apps |right_do| Select **l10n_it_mastrini** > Install


|

Upgrade / Aggiornamento
-----------------------


::

    cd $HOME
    # *** Tools installation & activation ***
    # Case 1: you have not installed zeroincombenze tools
    git clone https://github.com/zeroincombenze/tools.git
    cd $HOME/tools
    ./install_tools.sh -p
    source $HOME/devel/activate_tools
    # Case 2: you have already installed zeroincombenze tools
    cd $HOME/tools
    ./install_tools.sh -U
    source $HOME/devel/activate_tools
    # *** End of tools installation or upgrade ***
    # Odoo repository upgrade
    odoo_install_repository accounting -b 12.0 -o $HOME/12.0 -U
    vem amend $HOME/12.0/venv_odoo -o $HOME/12.0
    # Adjust following statements as per your system
    sudo systemctl restart odoo

From UI: go to:

|
|

Get involved / Ci mettiamo in gioco
===================================

Bug reports are welcome! You can use the issue tracker to report bugs,
and/or submit pull requests on `GitHub Issues
<https://github.com/powerp/accounting/issues>`_.

In case of trouble, please check there if your issue has already been reported.

|

Known issues / Roadmap
----------------------

None

ChangeLog History / Cronologia modifiche
----------------------------------------

12.0.8.7.22 (2021-04-21)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] POW - 121: fix popup bloccante

12.0.8.7.21 (2020-12-07)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] POW - 220: aggiornato campo type

12.0.8.7.20 (2020-08-31)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] AXI - 138: gestito l'esercizio fiscale

12.0.8.6.20 (2020-08-31)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] AXI-6/AXI-31: corretta gestione del riepilogo IVA per le note di credito

12.0.8.5.20 (2020-08-31)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] corretto XML ID del parent menu della voce Mastrini


12.0.8.5.19 (2020-08-28)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-6/AXI-28: Migliorata visualizzazione contropartite e controvalori



12.0.7.4.18 (2020-08-28)
~~~~~~~~~~~~~~~~~~~~~~~~
* [REF] AXI-109: Refactoring menu



12.0.7.4.17 (2020-08-26)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-6/AXI-28: Aggiunta visualizzazione contropartite e controvalori



12.0.6.3.16 (2020-08-20)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-117: Visualizzazione nome breve del registro invece del nome completo



12.0.5.3.15 (2020-08-20)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-126: Aggiunta visualizzazione stato delle registrazioni e relativo filtro



12.0.4.2.15 (2020-08-19)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-121: nascosti i pulsanti salva / abbandona



12.0.4.2.14 (2020-08-17)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-125: aggiunta colonna con tipo registrazione nella visualizzazione mastrini



12.0.3.2.13 (2020-08-07)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] aggiornamento della documentazione / history



12.0.3.2.12 (2020-08-06)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] aggiunta gestione segni in riepilogo IVA
* [IMP] AXI-6/AXI-31: Riepilogo IVA, prima implementazione funzionante



12.0.2.1.11 (2020-07-27)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-6/AXI-29: Colonne con date competenze ratei e risconti e controllo per visualizzarle e nasconderle



12.0.1.1.10 (2020-07-24)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-6/AXI-81: Etichette in Italiano e righe totali in grassetto
* [IMP] AXI-6/AXI-81: Righe totali in grassetto



12.0.0.2.9 (2020-07-24)
~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] AXI-6/AXI-81: inseriti nomi dei campi in Italiano
* [MOD] rimosso file superfluo
* [MOD] aggiornato numero versione



12.0.0.1.9 (2020-07-24)
~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] name overlapping



12.0.0.0.8 (2020-07-23)
~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-6/AXI-77: selezione date tramite intervalli (date_range)
* [MOD] AXI-6/AXI-78 + AXI-6/AXI-79: rimossi campi superflui dai wrapper, rimossa
eliminazione righe vecchie (fa Odoo da solo), rimossa associazione righe con wizard (non è necessaria)



12.0.0.0.7 (2020-07-22)
~~~~~~~~~~~~~~~~~~~~~~~
* [MOD] AXI-6/AXI-78 + AXI-6/AXI-79: completata implementazione totali in tabella
* [MOD] AXI-6/AXI-78 + AXI-6/AXI-79: prima implementazione totali in tabella



12.0.0.0.6 (2020-07-21)
~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] corretto errore nella funzione di ricerca anni fiscali all'interno del wizard dei mastrini



12.0.0.0.5 (2020-07-20)
~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-6/AXI-81 Cambiare colonne visualizzate - Completato
* [IMP] AXI-6/AXI-82 Scelta partner per conti di debito o credito



12.0.0.0.3 (2020-07-17)
~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] AXI-6/AXI-78 Saldi iniziali: miglioramento parte grafica - AXI-6/AXI-79 Saldi finali: miglioramento parte grafica - AXI-6/AXI-75 Proporre automaticamente esercizio contabile attuale
* [IMP] AXI-6/AXI-78 Saldi iniziali - AXI-6/AXI-79 Saldi finali - Implementato algoritmo di calcolo e bozza parte grafica
* [IMP] aggiornato numero di versione - aggiunta history, autori, ecc.
* [IMP] AXI-6/AXI-74 filtro per esercizio contabile - AXI-6/AXI-80 filtri su unica riga


12.0.0.0.2 (2020-07-09)
~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] corretto posizionamento saldo girnaliero
* [MOD] rimosso pulsante inutile dalla visualizzazione mastrini
* [FIX] eliminato problema del saldo giornaliero che scompariva quando le account.move.line venivano ricaricate


12.0.0.0.1 (2020-07-08)
~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] inseriti pulsanti apertura form di modifica, inseriti totali giornalieri totali per righe visualizzate, nascosti gli zero


12.0.0.0.0 (2020-07-07)
~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] prima implementazionezione funzionante


|
|

Credits / Didascalie
====================

Copyright
---------

Odoo is a trademark of `Odoo S.A. <https://www.odoo.com/>`__ (formerly OpenERP)



|

Authors / Autori
----------------

* `Axitec s.r.l. <https://www.axitec.it/>`__
* `SHS-AV s.r.l. <https://www.zeroincombenze.it/>`__
* `Didotech srl <http://www.didotech.com>`__


Contributors / Collaboratori
----------------------------

* Marco Tosato <marco.tosato@didotech.com>
* Alessandro Bobbo <bobbo@tecnosoft.it>
* Anderi Levin <andrei.levin@didotech.com>


Maintainer / Manutenzione
-------------------------


This module is maintained by the **Powerp**.

Questo modulo è mantenuto dalla rete di imprese **Powerp**.


|

----------------


|en| **Powerp** is the Italian Enterprises Network born in 2020, whose mission is promote use of Odoo to cover Italian law and markeplace.

`Powerp <http://www.powerp.it/>`__ distributes code under `LGPL <https://www.gnu.org/licenses/lgpl-3.0.html>`__ or `OPL <https://www.odoo.com/documentation/user/14.0/legal/licenses/licenses.html>`__ licenses.

Read carefully published README for more info about authors.

|it| `Powerp <http://www.powerp.it/>`__ è una rete di imprese, nata nel 2020 che rilascia moduli per la localizzazione italiana evoluta.

`Powerp <http://www.powerp.it/>`__ distribuisce il codice con licenze `LGPL <https://www.gnu.org/licenses/lgpl-3.0.html>`__ e `OPL <https://www.odoo.com/documentation/user/14.0/legal/licenses/licenses.html>`__

I soci fondatori sono:

* `Didotech s.r.l. <http://www.didotech.com>`__
* `SHS-AV s.r.l. <https://www.shs-av.com/>`__
* `Xplain s.r.l. <http://x-plain.it//>`__

Leggere con attenzione i file README per maggiori informazioni sugli autori.


|chat_with_us|


|

This module is part of accounting project.

Last Update / Ultimo aggiornamento: 2021-04-21

.. |Maturity| image:: https://img.shields.io/badge/maturity-Alfa-red.png
    :target: https://odoo-community.org/page/development-status
    :alt: Alfa
.. |Build Status| image:: https://travis-ci.org/powerp/accounting.svg?branch=12.0
    :target: https://travis-ci.org/powerp/accounting
    :alt: github.com
.. |license gpl| image:: https://img.shields.io/badge/licence-LGPL--3-7379c3.svg
    :target: http://www.gnu.org/licenses/lgpl-3.0-standalone.html
    :alt: License: LGPL-3
.. |license opl| image:: https://img.shields.io/badge/licence-OPL-7379c3.svg
    :target: https://www.odoo.com/documentation/user/9.0/legal/licenses/licenses.html
    :alt: License: OPL
.. |Coverage Status| image:: https://coveralls.io/repos/github/powerp/accounting/badge.svg?branch=12.0
    :target: https://coveralls.io/github/powerp/accounting?branch=12.0
    :alt: Coverage
.. |Codecov Status| image:: https://codecov.io/gh/powerp/accounting/branch/12.0/graph/badge.svg
    :target: https://codecov.io/gh/powerp/accounting/branch/12.0
    :alt: Codecov
.. |Tech Doc| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-docs-12.svg
    :target: https://wiki.zeroincombenze.org/en/Odoo/12.0/dev
    :alt: Technical Documentation
.. |Help| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-help-12.svg
    :target: https://wiki.zeroincombenze.org/it/Odoo/12.0/man
    :alt: Technical Documentation
.. |Try Me| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-try-it-12.svg
    :target: https://erp12.zeroincombenze.it
    :alt: Try Me
.. |OCA Codecov| image:: https://codecov.io/gh/OCA/accounting/branch/12.0/graph/badge.svg
    :target: https://codecov.io/gh/OCA/accounting/branch/12.0
    :alt: Codecov
.. |Odoo Italia Associazione| image:: https://www.odoo-italia.org/images/Immagini/Odoo%20Italia%20-%20126x56.png
   :target: https://odoo-italia.org
   :alt: Odoo Italia Associazione
.. |Zeroincombenze| image:: https://avatars0.githubusercontent.com/u/6972555?s=460&v=4
   :target: https://www.zeroincombenze.it/
   :alt: Zeroincombenze
.. |en| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/flags/en_US.png
   :target: https://www.facebook.com/Zeroincombenze-Software-gestionale-online-249494305219415/
.. |it| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/flags/it_IT.png
   :target: https://www.facebook.com/Zeroincombenze-Software-gestionale-online-249494305219415/
.. |check| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/check.png
.. |no_check| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/no_check.png
.. |menu| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/menu.png
.. |right_do| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/right_do.png
.. |exclamation| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/exclamation.png
.. |warning| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/warning.png
.. |same| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/same.png
.. |late| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/late.png
.. |halt| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/halt.png
.. |info| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/info.png
.. |xml_schema| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/iso/icons/xml-schema.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/iso/scope/xml-schema.md
.. |DesktopTelematico| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/ade/icons/DesktopTelematico.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/ade/scope/Desktoptelematico.md
.. |FatturaPA| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/ade/icons/fatturapa.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/ade/scope/fatturapa.md
.. |chat_with_us| image:: https://www.shs-av.com/wp-content/chat_with_us.gif
   :target: https://t.me/axitec_helpdesk

