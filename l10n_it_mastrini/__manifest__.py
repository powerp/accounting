{
    'name': 'l10n_it_mastrini',
    'summary': 'Mastrini',
    'version': '12.0.8.7.22',
    'category': 'Mastrini',
    'author': 'PowERP',
    'website': 'https://www.powerp.it/',
    'depends': [
        'base',
        'account',
        'date_range',
        'l10n_it_coa_base',
        'l10n_it_menu',
        'l10n_it_validations',
    ],
    'data': [
        'wizard/account_mastrini.xml',
        'views/ir_ui_menu.xml',
    ],
    'installable': True,
}
