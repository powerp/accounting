Piano dei conti multi-livello

Gestione della gerachia del piano dei conti, come in Odoo prima della 10.0
Il sistema permette la gestione di 3 livelli di conto, per ciascun livello è possibile definire la lunghezza in caratteri.

A livello più alto è possibile definire la natura del conto che può esssere:

* Attivo
* Passivo
* Ricavi
* Costi
* Conti d'ordine

per permettere di gestire il classico bilancio fiscale.

Questo modulo abilita il menà del tipo di conto che è normalmente nascosto in Odoo standard.
