#
# Copyright 2019-20 - SHS-AV s.r.l. <https://www.zeroincombenze.it/>
#
# Contributions to development, thanks to:
# * Antonio Maria Vigliotti <antoniomaria.vigliotti@gmail.com>
#
# License LGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
#
{
    'name': 'Chart Of Account Hierarchy',
    'summary': 'Chart Of Account Hierarchy, like Odoo prior 10.0',
    'version': '12.0.0.1.19',
    'category': 'Accounting',
    'author': 'powERP, Didotech srl, SHS-AV srl',
    'website': 'www.powerp.it',
    'license': 'LGPL-3',
    'depends': [
        'account',
        'base',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/profile_account.xml',
        'data/account_account_type.xml',
        'views/account_account_view.xml',
        'views/profile_account_view.xml',
        'views/config_view.xml',
    ],
    'installable': True,
    'maintainer': 'powERP'
    # 'post_init_hook': 'post_init',
}
