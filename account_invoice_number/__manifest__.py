#
# Copyright (c) 2021
#
{
    'name': 'Invoice Sequence Number',
    'summary': 'Add methods to get invoice number as integer '
               'extending ir_sequence and account_invoice',
    'version': '12.0.1.0.1',
    'category': 'Accounting',
    'author': 'powERP',
    'website': 'https://www.powerp.it',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'account',
    ],
    'data': [],
    'installable': True,
}
