12.0.1.0.1 (2021-04-07)
~~~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Fix funzionalità che estrae il numero intero della fattura dalla stringa

12.0.1.0.0 (2021-03-09)
~~~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Funzionalità che estrae il numero intero della fattura dalla stringa

