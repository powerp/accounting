12.0.1.1.5 (2021-03-27)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] Swapped amount_euro and statistic_amount_euro

12.0.0.1.5 (2021-03-26)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] Added statistic field

12.0.0.1.4 (2021-03-24)
~~~~~~~~~~~~~~~~~~~~~~~~
* [IMP] Added report

12.0.0.1.1 (2021-03-23)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] fix travis warning

