12.0.1.0.2 (2021-04-23)
~~~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Aggiornato messaggio di warning per i troppi codici iva

12.0.1.0.1 (2021-04-09)
~~~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Impostato il check sulle linee che hanno più codici iva

12.0.1.0.0 (2021-03-09)
~~~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Funzionalità che estende il wizard di stampa dei registri iva inserendo un warning sul controlo di numerazione

