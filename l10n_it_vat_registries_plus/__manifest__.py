#
# Copyright (c) 2021
#
{
    'version': '12.0.1.0.2',
    'name': 'ITA - Registri IVA Extended',
    'category': 'Localization/Italy',
    "author": "powERP",
    'website': 'https://www.powerp.it',
    'license': 'LGPL-3',
    "depends": [
        'account',
        'l10n_it_vat_registries',
        'account_invoice_number',
    ],
    "data": [
        'wizard/wizard_confirm_print_journal.xml'
    ],
    'installable': True,
}
