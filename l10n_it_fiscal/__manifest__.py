# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Italy - Fiscal localization by Zeroincombenze(R)',
    'version': '12.0.0.2.0',
    'depends': ['account', 'base_vat', 'base_iban'],
    'author': 'SHS-AV s.r.l.',
    'category': 'Localization/Account Charts',
    'data': [
        'data/l10n_it_chart_data.xml',
        'data/account.account.template.csv',
        'data/account.tax.group.csv',
        'data/account.tax.template.csv',
        'data/account.fiscal.position.template.csv',
        'data/account.chart.template.csv',
        'data/account_chart_template_data.xml',
    ],
    'maintainer': 'Antonio Maria Vigliotti',
    'license': 'AGPL-3',
    'website': 'http://www.zeroincombenze.it',
    'installable': True,
}
