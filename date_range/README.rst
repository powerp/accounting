
============================
|icon| Date Range 12.0.1.0.1
============================


**Manage all kind of date range**

.. |icon| image:: https://raw.githubusercontent.com/zeroincombenze/server-ux/12.0/date_range/static/description/icon.png

|Maturity| |Build Status| |Codecov Status| |license gpl| |Try Me|


.. contents::


Overview / Panoramica
=====================

|en| This module lets you define global date ranges that can be used to filter
your values in tree views.


|

|it| Impostazione intervallo date

Questo modulo permette di definire intervalli di date.

|

Usage / Utilizzo
----------------

Usage / Uso
===========

|menu| Settings > Technical > Date ranges > Date Range Types where
  you can create types of date ranges.

.. image:: https://raw.githubusercontent.com/zeroincombenze/server-ux/12.0/date_range/static/description/date_range_type_create.png
    :alt: create date range type

|menu| Technical > Date ranges >  Date Ranges where
  you can create date ranges.

.. image:: https://raw.githubusercontent.com/zeroincombenze/server-ux/12.0/date_range/static/description/date_range_create.png
    :alt: Date range creation

 It's also possible to launch a wizard from the 'Generate Date Ranges' menu.

.. image:: https://raw.githubusercontent.com/zeroincombenze/server-ux/12.0/date_range/static/description/date_range_wizard.png
    :alt: Date range wizard

The wizard is useful to generate recurring periods.

.. image:: https://raw.githubusercontent.com/zeroincombenze/server-ux/12.0/date_range/static/description/date_range_wizard_result.png
    :alt: Date range wizard result

Your date ranges are now available in the search filter for any date or datetime fields

Date range types are proposed as a filter operator

.. image:: https://raw.githubusercontent.com/OCA/server-tools/10.0/date_range/static/description/date_range_type_as_filter.png
    :alt: Date range type available as filter operator

Once a type is selected, date ranges of this type are porposed as a filter value

.. figure:: https://raw.githubusercontent.com/OCA/server-tools/10.0/date_range/static/description/date_range_as_filter.png
    :alt: Date range as filter value

And the dates specified into the date range are used to filter your result.

.. figure:: https://raw.githubusercontent.com/OCA/server-tools/10.0/date_range/static/description/date_range_as_filter_result.png
   :alt: Date range as filter result


|
|

Getting started / Come iniziare
===============================

|Try Me|


|

Installation / Installazione
----------------------------


+---------------------------------+------------------------------------------+
| |en|                            | |it|                                     |
+---------------------------------+------------------------------------------+
| These instruction are just an   | Istruzioni di esempio valide solo per    |
| example to remember what        | distribuzioni Linux CentOS 7, Ubuntu 14+ |
| you have to do on Linux.        | e Debian 8+                              |
|                                 |                                          |
| Installation is built with:     | L'installazione è costruita con:         |
+---------------------------------+------------------------------------------+
| `Zeroincombenze Tools <https://github.com/zeroincombenze/tools>`__         |
+---------------------------------+------------------------------------------+
| Suggested deployment is:        | Posizione suggerita per l'installazione: |
+---------------------------------+------------------------------------------+
| /home/odoo/12.0/server-ux/                                                 |
+----------------------------------------------------------------------------+

::

    cd $HOME
    git clone https://github.com/zeroincombenze/tools.git
    cd ./tools
    ./install_tools.sh -p
    source /opt/odoo/dev/activate_tools
    odoo_install_repository server-ux -b 12.0 -O zero
    venv_mgr create /opt/odoo/VENV-12.0 -O 12.0 -DI

From UI: go to:

|

Upgrade / Aggiornamento
-----------------------


+---------------------------------+------------------------------------------+
| |en|                            | |it|                                     |
+---------------------------------+------------------------------------------+
| When you want upgrade and you   | Per aggiornare, se avete installato con  |
| installed using above           | le istruzioni di cui sopra:              |
| statements:                     |                                          |
+---------------------------------+------------------------------------------+

::

    odoo_install_repository server-ux -b 12.0 -O zero -U
    venv_mgr amend /opt/odoo/VENV-12.0 -O 12.0 -DI
    # Adjust following statements as per your system
    sudo systemctl restart odoo

From UI: go to:

|

Support / Supporto
------------------


|Zeroincombenze| This module is maintained by the `SHS-AV s.r.l. <https://www.zeroincombenze.it/>`__


|
|

Get involved / Ci mettiamo in gioco
===================================

Bug reports are welcome! You can use the issue tracker to report bugs,
and/or submit pull requests on `GitHub Issues
<https://github.com/zeroincombenze/server-ux/issues>`_.

In case of trouble, please check there if your issue has already been reported.

Proposals for enhancement
-------------------------


|en| If you have a proposal to change this module, you may want to send an email to <cc@shs-av.com> for initial feedback.
An Enhancement Proposal may be submitted if your idea gains ground.

|it| Se hai proposte per migliorare questo modulo, puoi inviare una mail a <cc@shs-av.com> per un iniziale contatto.

ChangeLog History / Cronologia modifiche
----------------------------------------


12.0.1.0.1 (2020-03-31)
~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Parent in type (Beta) / Padre in tipo intervallo date
* [DOC] Documentation upgrade / Aggiornamento documentazione




|
|

Credits / Didascalie
====================

Copyright
---------

Odoo is a trademark of `Odoo S.A. <https://www.odoo.com/>`__ (formerly OpenERP)



|

Authors / Autori
----------------

* Acsone SA/NV <https://acsone.eu/>

Contributors / Collaboratori
----------------------------

* Laurent Mignon <laurent.mignon@acsone.eu>
* Alexis de Lattre <alexis.delattre@akretion.com>
* Miquel Raïch <miquel.raich@eficent.com>
* Andrea Stirpe <a.stirpe@onestein.nl>

Acknowledges / Riconoscimenti
-----------------------------

+-----------------------------------+-------------------------------------------+
| |en|                              | |it|                                      |
+-----------------------------------+-------------------------------------------+
| This software inherits from past  | Questo software eredita da versioni       |
| versions some parts of code. Even | passate alcune parti di codice. Anche     |
| if people did not actively        | se non hanno partecipato attivamente allo |
| participate to development, we    | allo sviluppo, noi siamo grati a tutte le |
| acknowledge them for their prior  | persone che precedentemente vi hanno      |
| contributions.                    | contribuito.                              |
+-----------------------------------+-------------------------------------------+

* Nikos Tsirintanis <ntsirintanis@therp.nl>

|

----------------


|en| **zeroincombenze®** is a trademark of `SHS-AV s.r.l. <https://www.shs-av.com/>`__
which distributes and promotes ready-to-use **Odoo** on own cloud infrastructure.
`Zeroincombenze® distribution of Odoo <https://wiki.zeroincombenze.org/en/Odoo>`__
is mainly designed to cover Italian law and markeplace.

|it| **zeroincombenze®** è un marchio registrato da `SHS-AV s.r.l. <https://www.shs-av.com/>`__
che distribuisce e promuove **Odoo** pronto all'uso sulla propria infrastuttura.
La distribuzione `Zeroincombenze® <https://wiki.zeroincombenze.org/en/Odoo>`__ è progettata per le esigenze del mercato italiano.


|chat_with_us|


|

This module is part of server-ux project.

Last Update / Ultimo aggiornamento: 2020-04-02

.. |Maturity| image:: https://img.shields.io/badge/maturity-Mature-green.png
    :target: https://odoo-community.org/page/development-status
    :alt: Mature
.. |Build Status| image:: https://travis-ci.org/zeroincombenze/server-ux.svg?branch=12.0
    :target: https://travis-ci.org/zeroincombenze/server-ux
    :alt: github.com
.. |license gpl| image:: https://img.shields.io/badge/licence-LGPL--3-7379c3.svg
    :target: http://www.gnu.org/licenses/lgpl-3.0-standalone.html
    :alt: License: LGPL-3
.. |license opl| image:: https://img.shields.io/badge/licence-OPL-7379c3.svg
    :target: https://www.odoo.com/documentation/user/9.0/legal/licenses/licenses.html
    :alt: License: OPL
.. |Coverage Status| image:: https://coveralls.io/repos/github/zeroincombenze/server-ux/badge.svg?branch=12.0
    :target: https://coveralls.io/github/zeroincombenze/server-ux?branch=12.0
    :alt: Coverage
.. |Codecov Status| image:: https://codecov.io/gh/zeroincombenze/server-ux/branch/12.0/graph/badge.svg
    :target: https://codecov.io/gh/zeroincombenze/server-ux/branch/12.0
    :alt: Codecov
.. |Tech Doc| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-docs-12.svg
    :target: https://wiki.zeroincombenze.org/en/Odoo/12.0/dev
    :alt: Technical Documentation
.. |Help| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-help-12.svg
    :target: https://wiki.zeroincombenze.org/it/Odoo/12.0/man
    :alt: Technical Documentation
.. |Try Me| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-try-it-12.svg
    :target: https://erp12.zeroincombenze.it
    :alt: Try Me
.. |OCA Codecov| image:: https://codecov.io/gh/OCA/server-ux/branch/12.0/graph/badge.svg
    :target: https://codecov.io/gh/OCA/server-ux/branch/12.0
    :alt: Codecov
.. |Odoo Italia Associazione| image:: https://www.odoo-italia.org/images/Immagini/Odoo%20Italia%20-%20126x56.png
   :target: https://odoo-italia.org
   :alt: Odoo Italia Associazione
.. |Zeroincombenze| image:: https://avatars0.githubusercontent.com/u/6972555?s=460&v=4
   :target: https://www.zeroincombenze.it/
   :alt: Zeroincombenze
.. |en| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/flags/en_US.png
   :target: https://www.facebook.com/Zeroincombenze-Software-gestionale-online-249494305219415/
.. |it| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/flags/it_IT.png
   :target: https://www.facebook.com/Zeroincombenze-Software-gestionale-online-249494305219415/
.. |check| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/check.png
.. |no_check| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/no_check.png
.. |menu| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/menu.png
.. |right_do| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/right_do.png
.. |exclamation| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/exclamation.png
.. |warning| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/warning.png
.. |same| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/same.png
.. |late| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/late.png
.. |halt| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/halt.png
.. |info| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/info.png
.. |xml_schema| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/iso/icons/xml-schema.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/iso/scope/xml-schema.md
.. |DesktopTelematico| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/ade/icons/DesktopTelematico.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/ade/scope/Desktoptelematico.md
.. |FatturaPA| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/ade/icons/fatturapa.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/ade/scope/fatturapa.md
.. |chat_with_us| image:: https://www.shs-av.com/wp-content/chat_with_us.gif
   :target: https://tawk.to/85d4f6e06e68dd4e358797643fe5ee67540e408b
