#
# Copyright (c) 2021
#
{
    'name': 'Sale Order Make Invoice Plus',
    'summary': 'Update default wizard handling invoice journal',
    'version': '12.0.1.0.0',
    'category': 'Accounting',
    'author': 'PowErp Srl',
    'website': '',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'account',
        'sale',
        'sale_order_type',
    ],
    'data': [],
    'installable': True,
}
