12.0.0.1.5 (2020-12-10)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] POW-189 Refactor of menu / Aggiornato menu, rimosso dal menu Contabiltà 

12.0.0.1.4 (2020-08-31)
~~~~~~~~~~~~~~~~~~~~~~~~
* [FIX] corretto XML ID del parent menu della voce Mastrini

12.0.0.1.3 (2020-08-28)
~~~~~~~~~~~~~~~~~~~~~~~~

* [REF] AXI-109 - Refactor of menu / Nuova impostazione del menu Contabilità
