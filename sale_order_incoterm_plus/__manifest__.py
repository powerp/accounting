# Copyright 2020 SHS-AV s.r.l. <https://www.zeroincombenze.it>
# Copyright 2020 Didotech s.r.l. <https://www.didotech.com>
#
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
#
{
    'name': 'Incoterms Plus',
    'summary': 'Incoterms Plus',
    'version': '12.0.2.1.2',
    'category': 'Accounting',
    'author': 'PowErp Srl',
    'website': '',
    'license': 'LGPL-3',
    'depends': [

        'base',

        # Dipendenza da 'sale' inserita esplicitamente perchè i campi inseriti
        # da questo modulo sono necessari per inserire correttamente il campo
        # con gli incoterm di default nell schermata delle impostazioni
        'sale',

        # Modulo sale_stock è il modulo che aggiunge il
        # campo "incoterm" nel sale.order
        'sale_stock',

        # Modulo che definisce il metodo di aggiornamento automatico
        # degli Incotemrs al cambio di partner, noi vogliamo estendere
        # questo metodo
        'sale_partner_incoterm'
    ],
    'data': [
        'security/ir.model.access.csv',
        'view/res_config.xml',
    ],
    'installable': True,
}
