
==========================
Incoterms Plus 12.0.2.1.2
==========================

Aggiunto Incoterm di default per "My Company". Quando viene fatto
un sale.order il programma cerca di impostare automaticamente
un Incoterm cercandolo prima nel partner indicato nel sale.order
e poi nella configurazione della company.