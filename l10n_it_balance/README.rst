
==================================
|icon| l10n_it_balance 12.0.0.2.58
==================================


**Acount balance**

.. |icon| image:: https://raw.githubusercontent.com/zeroincombenze/accounting/12.0/l10n_it_balance/static/description/icon.png

|Maturity| |Build Status| |Codecov Status| |license gpl| |Try Me|


.. contents::


Overview / Panoramica
=====================

|en| This software makes available three different kind of balance sheet:

* Trial balance
* Fiscal balance
* Opposite side balance

It designed for Italian enterprises but may be useful for other countries companies.


|

|it| Bilancio fiscale e legale per le imprese italiane.

Questo modulo permette di generare il bilancio fiscale di un'azienda secondo le normative italiane.
Sono disponibili 3 modelli di bilancio:

* Bilancio di verifica
* Bilancio ordinario
* Bilancio a sezioni contrapposte

Sono forniti numerosi filtri di selezione e stampa che permettono di adattare il bilancio a tutte
le esigenze fiscale e gestionali di un'impresa italiana.

Il software permette anche di effettuare la proiezione dei ratei e dei risconti.
Per ottenere questa funzione è necessario installare il modulo di registrazione date di competenze.



|
|

Getting started / Come iniziare
===============================

|Try Me|


|

Installation / Installazione
----------------------------


+---------------------------------+------------------------------------------+
| |en|                            | |it|                                     |
+---------------------------------+------------------------------------------+
| These instructions are just an  | Istruzioni di esempio valide solo per    |
| example; use on Linux CentOS 7+ | distribuzioni Linux CentOS 7+,           |
| Ubuntu 14+ and Debian 8+        | Ubuntu 14+ e Debian 8+                   |
|                                 |                                          |
| Installation is built with:     | L'installazione è costruita con:         |
+---------------------------------+------------------------------------------+
| `Zeroincombenze Tools <https://zeroincombenze-tools.readthedocs.io/>`__    |
+---------------------------------+------------------------------------------+
| Suggested deployment is:        | Posizione suggerita per l'installazione: |
+---------------------------------+------------------------------------------+
| $HOME/12.0                                                                 |
+----------------------------------------------------------------------------+

::

    cd $HOME
    # *** Tools installation & activation ***
    # Case 1: you have not installed zeroincombenze tools
    git clone https://github.com/zeroincombenze/tools.git
    cd $HOME/tools
    ./install_tools.sh -p
    source $HOME/devel/activate_tools
    # Case 2: you have already installed zeroincombenze tools
    cd $HOME/tools
    ./install_tools.sh -U
    source $HOME/devel/activate_tools
    # *** End of tools installation or upgrade ***
    # Odoo repository installation; OCB repository must be installed
    odoo_install_repository accounting -b 12.0 -O zero -o $HOME/12.0
    vem create $HOME/12.0/venv_odoo -O 12.0 -a "*" -DI -o $HOME/12.0

From UI: go to:

* |menu| Setting > Activate Developer mode 
* |menu| Apps > Update Apps List
* |menu| Setting > Apps |right_do| Select **l10n_it_balance** > Install


|

Upgrade / Aggiornamento
-----------------------


::

    cd $HOME
    # *** Tools installation & activation ***
    # Case 1: you have not installed zeroincombenze tools
    git clone https://github.com/zeroincombenze/tools.git
    cd $HOME/tools
    ./install_tools.sh -p
    source $HOME/devel/activate_tools
    # Case 2: you have already installed zeroincombenze tools
    cd $HOME/tools
    ./install_tools.sh -U
    source $HOME/devel/activate_tools
    # *** End of tools installation or upgrade ***
    # Odoo repository upgrade
    odoo_install_repository accounting -b 12.0 -o $HOME/12.0 -U
    vem amend $HOME/12.0/venv_odoo -o $HOME/12.0
    # Adjust following statements as per your system
    sudo systemctl restart odoo

From UI: go to:

|

Support / Supporto
------------------


|Zeroincombenze| This module is maintained by the `SHS-AV s.r.l. <https://www.zeroincombenze.it/>`__


|
|

Get involved / Ci mettiamo in gioco
===================================

Bug reports are welcome! You can use the issue tracker to report bugs,
and/or submit pull requests on `GitHub Issues
<https://github.com/zeroincombenze/accounting/issues>`_.

In case of trouble, please check there if your issue has already been reported.

Proposals for enhancement
-------------------------


|en| If you have a proposal to change this module, you may want to send an email to <cc@shs-av.com> for initial feedback.
An Enhancement Proposal may be submitted if your idea gains ground.

|it| Se hai proposte per migliorare questo modulo, puoi inviare una mail a <cc@shs-av.com> per un iniziale contatto.

ChangeLog History / Cronologia modifiche
----------------------------------------

12.0.0.2.58 (2021-03-11)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] inserito campo fattura data competenza bilancio

12.0.0.2.57 (2020-09-29)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] aggiornato typo nel nome del file di configurazione spreadsheet

12.0.0.2.56 (2020-09-28)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-153 - calcolo importi ratei/risconti
* [FIX] AXI-154 - crash

12.0.0.2.55 (2020-09-24)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-153 - calcolo importi ratei/risconti
* [FIX] AXI-154 - crash

12.0.0.2.54 (2020-09-24)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Refactor
* [FIX] AXI-136 - ordinamento righe bilancio a conti contrapposti
* [FIX] AXI-128 - inversione di segno caso 'sempre'
* [FIX] AXI-129 - inversione di segno caso 'se richiesto'

12.0.0.2.53 (2020-09-17)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Calcolo utile

12.0.0.2.52 (2020-09-10)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] travis - verifica

12.0.0.2.51 (2020-09-10)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-136 - ordinamento righe bilancio a conti contrapposti
* [FIX] AXI-128 - inversione di segno caso 'sempre'
* [FIX] AXI-129 - inversione di segno caso 'se richiesto'

12.0.0.2.50 (2020-08-28)
~~~~~~~~~~~~~~~~~~~~~~~~

* [REF] AXI-109 - Aggiornato menu

12.0.0.2.49 (2020-08-19)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-108 - Rimosso vincolo su flag ecslusi conti iniziali

12.0.0.2.48 (2020-08-18)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-115 - Aggiornato ordinamento sul codice conto

12.0.0.2.47 (2020-08-17)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-55 / AXI-104 - Cambiato tipo di campo per inversione di segno

12.0.0.2.46 (2020-08-07)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-17 - Download immediato file xls per bilancio ordinario e di verifica

12.0.0.2.45 (2020-08-06)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-17 - Aggiornato larghezza colonne per bilancio a conti contrapposti

12.0.0.2.44 (2020-08-03)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-17 - Aggiornato formato numerico e allineamenti

12.0.0.2.43 (2020-07-29)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-90 - Indicato periodo e non anno nelle proiezioni ratei/risconti


12.0.0.2.42 (2020-07-29)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] AXI-89 - Alcuni conti sono spostati tra attivo e passivo
* [FIX] AXI-103 - Gestione warning profilo contabile


|
|

Credits / Didascalie
====================

Copyright
---------

Odoo is a trademark of `Odoo S.A. <https://www.odoo.com/>`__ (formerly OpenERP)



|

Authors / Autori
----------------

* `SHS-AV s.r.l. <https://www.zeroincombenze.it/>`__
* `Didotech srl <http://www.didotech.com>`__


Contributors / Collaboratori
----------------------------

* Antonio Maria Vigliotti <antoniomaria.vigliotti@gmail.com>
* Fabio Giovannelli <fabio.giovannelli@didotech.com>


Maintainer / Manutenzione
-------------------------




|

----------------


|en| **zeroincombenze®** is a trademark of `SHS-AV s.r.l. <https://www.shs-av.com/>`__
which distributes and promotes ready-to-use **Odoo** on own cloud infrastructure.
`Zeroincombenze® distribution of Odoo <https://wiki.zeroincombenze.org/en/Odoo>`__
is mainly designed to cover Italian law and markeplace.

|it| **zeroincombenze®** è un marchio registrato da `SHS-AV s.r.l. <https://www.shs-av.com/>`__
che distribuisce e promuove **Odoo** pronto all'uso sulla propria infrastuttura.
La distribuzione `Zeroincombenze® <https://wiki.zeroincombenze.org/en/Odoo>`__ è progettata per le esigenze del mercato italiano.


|chat_with_us|


|

This module is part of accounting project.

Last Update / Ultimo aggiornamento: 2021-03-11

.. |Maturity| image:: https://img.shields.io/badge/maturity-Alfa-red.png
    :target: https://odoo-community.org/page/development-status
    :alt: Alfa
.. |Build Status| image:: https://travis-ci.org/zeroincombenze/accounting.svg?branch=12.0
    :target: https://travis-ci.org/zeroincombenze/accounting
    :alt: github.com
.. |license gpl| image:: https://img.shields.io/badge/licence-LGPL--3-7379c3.svg
    :target: http://www.gnu.org/licenses/lgpl-3.0-standalone.html
    :alt: License: LGPL-3
.. |license opl| image:: https://img.shields.io/badge/licence-OPL-7379c3.svg
    :target: https://www.odoo.com/documentation/user/9.0/legal/licenses/licenses.html
    :alt: License: OPL
.. |Coverage Status| image:: https://coveralls.io/repos/github/zeroincombenze/accounting/badge.svg?branch=12.0
    :target: https://coveralls.io/github/zeroincombenze/accounting?branch=12.0
    :alt: Coverage
.. |Codecov Status| image:: https://codecov.io/gh/zeroincombenze/accounting/branch/12.0/graph/badge.svg
    :target: https://codecov.io/gh/zeroincombenze/accounting/branch/12.0
    :alt: Codecov
.. |Tech Doc| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-docs-12.svg
    :target: https://wiki.zeroincombenze.org/en/Odoo/12.0/dev
    :alt: Technical Documentation
.. |Help| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-help-12.svg
    :target: https://wiki.zeroincombenze.org/it/Odoo/12.0/man
    :alt: Technical Documentation
.. |Try Me| image:: https://www.zeroincombenze.it/wp-content/uploads/ci-ct/prd/button-try-it-12.svg
    :target: https://erp12.zeroincombenze.it
    :alt: Try Me
.. |OCA Codecov| image:: https://codecov.io/gh/OCA/accounting/branch/12.0/graph/badge.svg
    :target: https://codecov.io/gh/OCA/accounting/branch/12.0
    :alt: Codecov
.. |Odoo Italia Associazione| image:: https://www.odoo-italia.org/images/Immagini/Odoo%20Italia%20-%20126x56.png
   :target: https://odoo-italia.org
   :alt: Odoo Italia Associazione
.. |Zeroincombenze| image:: https://avatars0.githubusercontent.com/u/6972555?s=460&v=4
   :target: https://www.zeroincombenze.it/
   :alt: Zeroincombenze
.. |en| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/flags/en_US.png
   :target: https://www.facebook.com/Zeroincombenze-Software-gestionale-online-249494305219415/
.. |it| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/flags/it_IT.png
   :target: https://www.facebook.com/Zeroincombenze-Software-gestionale-online-249494305219415/
.. |check| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/check.png
.. |no_check| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/no_check.png
.. |menu| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/menu.png
.. |right_do| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/right_do.png
.. |exclamation| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/exclamation.png
.. |warning| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/warning.png
.. |same| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/same.png
.. |late| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/late.png
.. |halt| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/halt.png
.. |info| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/awesome/info.png
.. |xml_schema| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/iso/icons/xml-schema.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/iso/scope/xml-schema.md
.. |DesktopTelematico| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/ade/icons/DesktopTelematico.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/ade/scope/Desktoptelematico.md
.. |FatturaPA| image:: https://raw.githubusercontent.com/zeroincombenze/grymb/master/certificates/ade/icons/fatturapa.png
   :target: https://github.com/zeroincombenze/grymb/blob/master/certificates/ade/scope/fatturapa.md
.. |chat_with_us| image:: https://www.shs-av.com/wp-content/chat_with_us.gif
   :target: https://t.me/axitec_helpdesk

