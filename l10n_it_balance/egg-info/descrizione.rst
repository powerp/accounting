Bilancio fiscale e legale per le imprese italiane.

Questo modulo permette di generare il bilancio fiscale di un'azienda secondo le normative italiane.
Sono disponibili 3 modelli di bilancio:

* Bilancio di verifica
* Bilancio ordinario
* Bilancio a sezioni contrapposte

Sono forniti numerosi filtri di selezione e stampa che permettono di adattare il bilancio a tutte
le esigenze fiscale e gestionali di un'impresa italiana.

Il software permette anche di effettuare la proiezione dei ratei e dei risconti.
Per ottenere questa funzione è necessario installare il modulo di registrazione date di competenze.

