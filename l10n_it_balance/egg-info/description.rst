This software makes available three different kind of balance sheet:

* Trial balance
* Fiscal balance
* Opposite side balance

It designed for Italian enterprises but may be useful for other countries companies.
