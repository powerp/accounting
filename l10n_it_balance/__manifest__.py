#
# Copyright 2018-19 - SHS-AV s.r.l. <https://www.zeroincombenze.it>
#
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).
#
{
    'name': 'l10n_it_balance',
    'summary': 'Acount balance',
    'version': '12.0.0.2.61',
    'category': 'Generic Modules/Accounting',
    'author': 'SHS-AV s.r.l.',
    'website': 'https://www.zeroincombenze.it/',
    'depends': ['base',
                'account',
                'date_range',
                'account_fiscal_year',
                'l10n_it_coa_base',
                'account_accrual_dates',
                'l10n_it_menu',
                'l10n_it_validations',
                ],
    'data': [
        'security/ir.model.access.csv',
        'reports/templates/trial_balance.xml',
        'reports/templates/ordinary_balance.xml',
        'reports/templates/opposite_balance.xml',
        'wizard/generate_balance_view.xml',
        'wizard/export_xls.xml',
        'wizard/export_opposite_xls.xml',
        'views/account_balance_view.xml',
        'views/ir_ui_menu.xml',
        'views/account_invoice_view.xml',
        'views/account_move_view.xml',
        'views/account_view.xml',
    ],
    'installable': True,
}
