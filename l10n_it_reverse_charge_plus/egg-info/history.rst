12.0.1.2.7 (2021-03-18)
~~~~~~~~~~~~~~~~~~~~~~~~

[FIX] Error when payment invoice: function invoice_validate @multi


12.0.1.2.6 (2021-02-17)
~~~~~~~~~~~~~~~~~~~~~~~~

[REF] Clone OCA module
[FIX] Mixed RC and ordinary VAT line in single vendor bill
[FIX] Self invoice account move lines
