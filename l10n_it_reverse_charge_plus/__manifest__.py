# Copyright 2017 Davide Corio
# Copyright 2017 Alex Comba - Agile Business Group
# Copyright 2017 Lorenzo Battistini - Agile Business Group
# Copyright 2017 Marco Calcagni - Dinamiche Aziendali srl
# Copyright 2021 Antonio M. Vigliotti - SHS-Av srl
# Copyright 2021 powERP, Didotech srl, SHS-AV srl <https://www.powerp.it>
# Copyright 2021 SHS-AV s.r.l. <https://www.zeroincombenze.it>
# Copyright 2021 Didotech s.r.l. <https://www.didotech.com>
#
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
#

{
    'name': 'ITA - Inversione contabile',
    'version': '12.0.1.2.7',
    'category': 'Localization/Italy',
    'summary': 'Inversione contabile',
    'author': 'powERP, Odoo Italia Network, Odoo Community Association (OCA)',
    'license': 'LGPL-3',
    'website': 'www.powerp.it',
    'depends': [
        'account',
        'account_cancel',
        'l10n_it_account_tax_kind',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/rc_type.xml',
        'views/account_invoice_view.xml',
        'views/account_fiscal_position_view.xml',
        'views/account_rc_type_view.xml',
        'security/reverse_charge_security.xml',
    ],
    'installable': True,
    'maintainer': 'powERP enterprise network'
}
