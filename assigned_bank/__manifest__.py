# -*- coding: utf-8 -*-
#
# Copyright 2019-20 - SHS-AV s.r.l. <https://www.zeroincombenze.it/>
#
# Contributions to development, thanks to:
# * Antonio Maria Vigliotti <antoniomaria.vigliotti@gmail.com>
#
# License LGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
#
{
    'name': 'Assigned dfBanks',
    'summary': 'Assign internal banks to customer or supplier',
    'version': '12.0.0.2.1',
    'category': 'Generic Modules/Accounting',
    'author': 'SHS-AV s.r.l.',
    'website': 'https://www.zeroincombenze.it/',
    'depends': ['base',
                'account'],
    'data': [
        'views/res_config_settings_views.xml',
        'views/partner_view.xml',
    ],
    'installable': True,
}
