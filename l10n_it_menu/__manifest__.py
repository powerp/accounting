# Copyright 2020-21 powERP <https://www.powerp.it>
# Copyright 2020-21 SHS-AV s.r.l. <https://www.zeroincombenze.it>
# Copyright 2020-21 Didotech s.r.l. <https://www.didotech.com>
#
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
#
{
    'name': 'l10n_it_menu',
    'summary': 'Accounting menu',
    'version': '12.0.0.1.5',
    'category': 'Generic Modules/Accounting',
    'author': 'powERP, Didotech srl, SHS-AV srl',
    'website': 'https://www.powerp.it',
    'depends': ['base',
                'account',
                ],
    'data': [
        'views/accounting_menuitem.xml',
    ],
    'installable': True,
    'maintainer': 'powERP enterprise network'
}
