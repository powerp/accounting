Chart of Account and Tax codes
------------------------------

This module load an empty chart of account and empty tax codes
but it set journals and internal Odoo data.

Use this module when end user wants to insert the own chart of acount by itself.

Warning! Due Odoo structure following data are inserted:

* Customer account, code CLI
* Supplier account, code FOR
* Revenue account, code RCV
* Expense account, code CST
* Bank account, code BCA
* VAT account, code IVA
* Tax code 22%
