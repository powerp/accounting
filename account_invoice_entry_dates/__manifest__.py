# -*- coding: utf-8 -*-
#
# Copyright 2017-20 - SHS-AV s.r.l. <https://www.zeroincombenze.it/>
#
# Contributions to development, thanks to:
# * Antonio Maria Vigliotti <antoniomaria.vigliotti@gmail.com>
#
# License LGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
#
{
    'name': 'Invoice entry dates',
    'summary': 'Registration, vat/balance application dates',
    'version': '12.0.2.6.29',
    'category': 'Accounting',
    'author': 'SHS-AV s.r.l.',
    'website': 'https://www.zeroincombenze.it/servizi-le-imprese/',
    'license': 'LGPL-3',
    'depends': [
        'account',
        'l10n_it_fiscal_payment_term',
        'account_fiscal_year',
        'account_invoice_13_more',
    ],
    'data': [
        'security/ir.model.access.csv',
        'view/account_journal_view.xml',
        'view/account_move_view.xml',
        'view/account_invoice_view.xml',
    ],
    'installable': True,
    'maintainer': 'Antonio Maria Vigliotti',
}
