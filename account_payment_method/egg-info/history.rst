12.0.0.2.1 (2021-02-26)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Show flag about check client bank account / Esposto il flag di Conto bancario necessario

12.0.0.1.2 (2021-02-10)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] No som default methods / Rimossi alcuni metodi pagamento predefiniti


12.0.0.1.1 (2020-10-12)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Added credit/debit field / Aggiunto il campo debito/credito
