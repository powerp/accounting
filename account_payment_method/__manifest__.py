#
# Copyright 2020-21 SHS-AV s.r.l. <https://www.zeroincombenze.it>
#
# Contributions to development, thanks to:
# * Antonio Maria Vigliotti <antoniomaria.vigliotti@gmail.com>
#
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
#
{
    'name': 'Account Payment Method',
    'summary': 'Extend payment method model',
    'version': '12.0.0.2.1',
    'category': 'Generic Modules/Accounting',
    'author': 'powERP, Didotech srl, SHS-AV srl',
    'website': 'www.powerp.it',
    'depends': ['base', 'account'],
    'data': [
        'views/payment_method_view.xml',
        'data/payment_method.xml',
    ],
    'installable': True,
    'maintainer': 'powERP',
    'development_status': 'Alpha',
}
