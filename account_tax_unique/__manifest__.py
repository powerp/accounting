#
# Copyright (c) 2020
#
{
    'name': 'Account invoice tax unique',
    'summary': 'Account Invoice Tax Unique',
    'version': '12.0.1.0.4',
    'category': 'Generic Modules/Accounting',
    'author': 'Didotech.srl - SHS-AV s.r.l.',
    'website': 'http://www.didotech.com/',
    'depends': ['base',
                'account',
                'sale',
                ],
    'data': [
    ],
    'installable': True,
}
