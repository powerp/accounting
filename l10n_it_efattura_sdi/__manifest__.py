# -*- coding: utf-8 -*-
{
    'name': "Generic SDI",
    'summary': """Generic module used for integration with SDI
        """,

    'description': """
        Generic module used for integration with SDI
    """,

    'author': "Didotech s.r.l.",
    'website': "http://www.didotech.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Generic Modules/Accounting',
    'version': '2.4.4',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'l10n_it_fatturapa_in',  # l10n-italy
        'l10n_it_fatturapa_out',  # l10n-italy
    ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/cron.xml',
        'views/account_view.xml',
        'views/attachment_view.xml',
        'views/company_view.xml',
        'views/partner_view.xml',
        'wizard/wizard_import_invoice_view.xml'
    ],

    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}
