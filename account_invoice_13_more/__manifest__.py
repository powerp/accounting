#
# Copyright (c) 2020
#
{
    'name': 'Account Invoice 13 more',
    'summary': 'Account Invoice 13 more',
    'version': '12.0.2.3.10',
    'category': 'Accounting',
    'author': 'powERP',
    'website': 'www.powerp.it',
    'license': 'LGPL-3',
    'depends': [
        'account',
    ],
    'data': [
        # 'security/ir.model.access.csv',
        'views/account_move_view.xml',
    ],
    'installable': True,
}
