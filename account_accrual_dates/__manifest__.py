# Copyright 2019-20 - SHS-AV s.r.l. <https://www.zeroincombenze.it/>
#
# Contributions to development, thanks to:
# * Antonio Maria Vigliotti <antoniomaria.vigliotti@gmail.com>
#
# License LGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
#
{
    'name': 'Account accrual dates',
    'version': '12.0.1.0.0',
    'category': 'Accounting & Finance',
    'author': 'SHS-AV s.r.l.',
    'website': 'https://www.zeroincombenze.it/',
    'license': 'LGPL-3',
    'summary': 'Adds start/end accrual dates on invoice lines and move lines',
    'depends': [
        'account',
    ],
    'data': [
        'views/account_invoice.xml',
        'views/account_move.xml',
    ],
    'installable': True,
}
