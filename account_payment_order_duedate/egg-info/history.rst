12.0.1.1.1 (2021-02-12)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Set maximum calculation  / Impostato calcolo massimale

12.0.0.2.1 (2021-02-11)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Filter journal with set default  / Impostato nel wizard il filtro sul registro impostando un default se è uno solo

12.0.0.1.9 (2021-01-15)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Filter payment mode with payment method set default  / Impostato nel wizard il filtro sul modo di pagamento impostando un default se è uno solo

12.0.0.1.8 (2020-12-28)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Filter bank journal type in creation wizard  / Impostato nel wizard il filtro sul registro con tipo 'banca'

12.0.0.1.7 (2020-12-28)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Filter payment mode with payment method in payment order creation wizard  / Impostato nel wizard il filtro sul modo di pagamento dal metodo delle scadenze

12.0.0.1.6 (2020-11-27)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Fix match with payment method  / Corretto il match con i metodi di pagamento

12.0.0.1.5 (2020-11-24)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Fix check on payment method  / Verificato e modificato il controllo sul metodo di pagamento

12.0.0.1.4 (2020-11-20)
~~~~~~~~~~~~~~~~~~~~~~~~

* [FIX] Fix parameter in add call  / Corretto il parametro nella chiamata al metodo di inserimento in distinta

12.0.0.1.3 (2020-11-06)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Added wizard  / Aggiunto il wizard per inserimento righe nella distinta dalle scadenze

12.0.0.1.2 (2020-10-20)
~~~~~~~~~~~~~~~~~~~~~~~~

* [IMP] Added wizard  / Aggiunto il wizard per la generazione della distinta dalle scadenze
