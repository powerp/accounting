#
# Copyright (c) 2020
{
    'name': 'Account Payment Order Duedate',
    'summary': 'Account Payment Order extension',
    'version': '12.0.1.2.1',
    'category': 'Accounting',
    'author': 'Didotech srl',
    'website': 'https://www.didotech.com/',
    'license': 'LGPL-3',
    'depends': [
        'base',
    ],
    'data': [
        # 'views/action_order_generate.xml',
        # 'views/action_order_add_move_lines.xml',
        # 'wizard/wizard_account_payment_order_generate.xml',
        # 'wizard/wizard_account_payment_order_add_move_lines.xml'
    ],
    'installable': False,
}
